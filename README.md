# Cython_intro


### When should implement in Cython
1. gặp trường hợp vòng for lớn, bắt buộc phải xử lý tuần tự [link](https://towardsdatascience.com/use-cython-to-get-more-than-30x-speedup-on-your-python-code-f6cb337919b6)
2. 
### Getting started

1. install [anaconda](https://www.anaconda.com/)
2. create new conda env
3. `pip install -U Cython`
4. create `.pyx` file and define some function
5. create `setup.py`
6. run `sh compile.sh`
7. chỉ cần giữ lại `<module_name>.cpython-3*-x86_64-linux-gnu.so` file để  import 

### useful link 
1. https://codelearn.io/sharing/cython-cach-de-code-python-chay-nhanh
2. https://cython.readthedocs.io/en/latest/src/tutorial/cython_tutorial.html
3. https://medium.com/mathematicallygifted/cython-use-it-to-speed-up-python-code-with-examples-cd5a90b32fc7
